#include "utils.h"


void print(char *str) {
    print_str_coloured(str, DEFAULT_COLOUR);
}

void println(char *str) {
    print_str_coloured(str, DEFAULT_COLOUR);
    print("\n");
}

void print_str_coloured(char *str, unsigned char colour) {
    int offset = get_cursor();
    int i = 0;
    while (str[i] != '\0') {
        if (offset >= MAX_ROWS * MAX_COLS * 2) {
            offset = scroll_ln(offset);
        }
        if (str[i] == '\n') {
            offset = move_offset_to_new_line(offset);
        } else {
            print_char_at(str[i], offset, colour);
            offset += 2;
        }
        i++;
    }
    set_cursor(offset);
}