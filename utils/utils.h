#pragma once

#include <stdint.h>
#include "../drivers/vga.h"

void print_str_coloured(char *str, unsigned char colour);
void print(char *str);
void println(char *str);