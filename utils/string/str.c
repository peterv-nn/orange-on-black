#include "str.h"


char* int_to_str(int number) {
    bool is_negative = (number < 0);
    int length = 2;
    int num = (is_negative) ? -1*number : number;
    while((num /= 10) > 0) {
        ++length;
    } 

    int offset = (is_negative) ? 1 : 0;
    char* str =  mem_alloc((length + offset)*sizeof(char));

    num = (is_negative) ? -1*number : number;
    int idx = 1;
    do {
        str[(length + offset) - idx - 1] = (num % 10) + '0'; 
        ++idx;
    } while(num /= 10);
    str[idx] = '\0';

    if(is_negative) {
        str[0] = '-';
    }

    return str;
}