all: run

C_SOURCES = ${wildcard kernel/*.c drivers/*.c in_out/*.c std/*.c interrupt/*.c utils/*.c utils/string/*.c memory/*.c}
HEADERS  = ${wildcard kernel/*.h drivers/*.h in_out/*.h std/*.h interrupt/*.h utils/*.h utils/string/*.h memory/*.h}
OBJ_FILES = ${C_SOURCES:.c=.o interrupt/interrupt.o}

kernel.bin: kernel/kernel_entry.o ${OBJ_FILES}
	x86_64-elf-ld -m elf_i386 -o $@ -Ttext 0x100000 $^ --oformat binary

os-image.bin: boot_sector/boot_sector.bin kernel.bin
	cat $^ > $@

run: os-image.bin 
	qemu-system-i386 -fda $<

# symbol file for debugging
kernel.elf: kernel/kernel_entry.o ${OBJ_FILES}
	x86_64-elf-ld -m elf_i386 -o $@ -Ttext 0x100000 $^

debug: os-image.bin kernel.elf
	qemu-system-i386 -s -S -fda os-image.bin -d guest_errors,int &
	gdb -ex "target remote localhost:1234" -ex "symbol-file kernel.elf" -ex "break start_kernel"


%.o: %.c ${HEADERS}
	x86_64-elf-gcc -m32 -g -ffreestanding -c $< -o $@ 

%.o: %.asm
	nasm $< -f elf -o $@

%.bin: %.asm
	nasm $< -f bin -o $@


clean:
	$(RM) *.bin *.o *.elf
	$(RM) boot_sector/*.bin
	$(RM) kernel/*.o *.bin
	$(RM) drivers/*.o
	$(RM) in_out/*.o
	$(RM) std/*.o
	$(RM) interrupt/*.o
	$(RM) utils/*.o
	$(RM) utils/string/*.o
	$(RM) memory/*.o