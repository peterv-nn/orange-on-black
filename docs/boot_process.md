# OS Dev

## The process of booting up

When we turn on our computer it must start up somehow.
The very first thing that will happen is the loading of the firmware **B**asic **I**nput/**O**utput **S**ystem. This comes pre-installed on every IBM PC (or compatible) system board. The BIOS gives you a collection of software routines that makes it possible to have a basic control over the computer's essential IO devices, like the keyboard, screen or hdd.

Right after the BIOS loaded it will perform a **P**ower **O**n **S**elf **T**est, which is responsible for ensuring that your hardware is working correctly.

After the POST the BIOS will start loading the **O**perating **S**ystem.

### Loading the Operating System

BIOS will go trough each of the storage devices in its mission of looking for a **M**aster **B**oot **R**ecord. 

So what makes a minimal MBR?

#### MBR

1. The MBR has to be the first sector of any disks
2. It has to be 512 byte long
3. The last two bytes must contain the magic number 0xaa55

```h
           eb  fe  00  00  00  00  00  00  00  00  00  00  00  00  00  00
           00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00
*
           00  00  00  00  00  00  00  00  00  00  00  00  00  00  55  aa
```

---
**Notice**

*The last two bytes are in reverse order. This is not a typo but a precuriality of the x86 architecture.* 

*It uses little endian byte ordering.*

---

The above hex dump is an abolute minimal MBR, that does nothing apart from an endless jump. 
If you would want to implement yourself you would need an assembler.

```asm
jmp $                   ; performing endless jump


; assembler instructions not executable
times 510-($-$$) db 0   ; padding with zeroes
dw 0xaa55               ; the magic number
```


#### Welcome to 16bit Real Mode

Congratulations! You have arrived to the land of 16bits the real deal.
Lets bisect the title a bit to explore what it actually mean.
`Real Mode` referes to the fact that memory addresses always correspond to actual locations in memory. 
`16bit` refers to the length with which the CPU instructions can work. This has some important implications, namely that the maximum addressable memory is 64KB. This can be extended to little over 1MB by using [segmentation](segmentation.md), however the actual usable memory will be much less.

So where is our code actually located in memory?

| ![MemoryMap after boot](memory_layout_after_boot.png "Memory map after boot.") |
|:--:|
|Fig. 1: *Source: [cs.bham.ac.uk](https://www.cs.bham.ac.uk/~exr/lectures/opsys/10_11/lectures/os-dev.pdf)*|


As we have discussed before the first thing that loads on boot is the BIOS.
It will always occupy the bottom of the memory, starting with the **I**nterrupt **V**ector **T**able, followed by the **B**ios **D**ata **A**rea. These two areas are actively being used by the BIOS so you really should not attempt to store anything there. The boot sector will be loaded on `0x7c00` 
The **E**xtended **B**ios **D**ata **A**rea has variable size and also should not be overwritten. 

As you can see the BIOS is already loaded and available for use. We can interact with through interrupts, and it gives support to a wide range of applications for example [loading data from disk](../boot_sector/disk_load.asm) or [printing to the screen](../boot_sector/print_string.asm).

It is possible to build an OS in real mode (DOS) but the resource limits are very tight so you would not want to spend too much time around here. 
You probably want to switch to 32bit protected mode as soon as possible.
However before we would do so, we should implement and load our kernel from disk as loading from disk is a lot easier with the help of the BIOS.

#### Loading the Kernel

The very first thing that we will need is a kernel that we can load.
As an example here we will just stick to the do nothing kernel and we will just try to focus on points where things can go wrong.

```C
void start_kernel() {
}
```

The above is probably the simples code that we can write as a kernel in C. It will not do much, yet it is still useful to present it here as there are a few gotchas that we need to talk about.

When we will try to call our kernel later on from 32bit protected mode we will simply do so by referring to the memory address it was loaded to and just start calling instructions we find there.
In the above case this is non-problematic but if we have anything else defined above our `start_kernel` function then we will end up with some weird behaviour.

To prevent this we can add a simple bootstrap code that will always be linked in front of the kernel. 

```asm
global _start;
[bits 32]

_start:
    [extern start_kernel] ; Define calling point. Must have same name as kernel.c entry function
    call start_kernel     ; Calls the C function. The linker will know where it is placed in memory
    jmp $
```

Compiling and linking can be done the following way:

```
nasm kernel_entry.asm -f elf -o kernel_entry.o

x86_64-elf-gcc -g -m32 -ffreestanding -c kernel.c -o kernel.o 

x86_64-elf-ld -m elf_i386 -o kernel.bin -Ttext 0x1000 kernel_entry.o kernel.o --oformat binary
```

---
**Notice**

*These instructions are specific for the compiler and tools that you are using. It is very likely that you will need to install a corss compiler as well. There are some very good tutorials on OSDev Wiki on how to do this.*

[OSDev Wiki - Setting up Cross-Compiler](https://wiki.osdev.org/GCC_Cross-Compiler)

---


This will build us a kernel binary in an elf format.
Next task on the list is loading it.

Loading the kernel is fairly easy with the help of the BIOS interrupt `0x13`, you will need some initial setup such as where you want to read from (cylinder and head), the number of sectors and where you want to load the data to. [Example](../boot_sector/disk_load.asm).

One of the more important questions is probably where to load the kernel in memory. 
At the beginning while our kernel is small it probably will not matter too much, but remember that there are places in memory where you should not write anything. Refere back to the [memory map figure](./memory_layout_after_boot.png) discussed previously.

For `Orange on Black OS` the kernel is loaded at 1MiB location, which you can see below:

```asm
...

    KERNEL_BASE   equ 0xffff      ; Loading the kernel at 1MB
    KERNEL_OFFSET equ 0x10        ; KERNEL_BASE * 16 + KERNEL_OFFSET = 0x100000
...

[bits 16]
load_kernel:
    mov bx, KERNEL_OFFSET
    mov dh, 32
    mov dl, [BOOT_DRIVE]
    mov ax, KERNEL_BASE
    mov es, ax
    xor ax, ax
    call disk_load
    ret
```

---
**Notice**

  *Make sure you load enough `dh` sectors that your kernel fit. If you don't then that it can lead to some funny looking undefined behaviour.*
  
  *I know... from experience.*

---


#### Switching to 32bit protected mode

This will need some preparation.

1. Check that A20 line is enabled (and enable it)
2. Set up GDT
3. Disable Interrupts
4. Jump to kernel code

