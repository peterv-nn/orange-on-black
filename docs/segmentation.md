## Segmentation

We need to go back in time quite a bit to understand why memory segmentation was introduced. 

Back in the day in 1978 when the 8086 was introduced it had a 16bit processor (16bit registers) and 20bit address line. The 20bit address line at that time gave access to 1MB of addressable memory which was considered huge. 
The problem was that 20bits is not byte compatible so it was somewhat tricky to use the 16bit registers to address the available 1MB of memory, so IBM engineers had to come up with a solution.

In real mode to access logical memory your addresses will take the following form `B:O` where `B` is the base and `O` is the offset. 
The physical address then will be calculated as follows:

`Physical address = B<<4 + O`

1. Left shift by 4 the 16bit address of base
2. Add the offset

```
B is 0x1000h
O is 0x0020h

  0001 0000 0000 0000 0000
+      0000 0000 0010 0000
--------------------------------------
  0001 0000 0000 0010 0000

= 0x10020h
```

With the 80286 a new way of segmentation was introduced for 32bit protected mode via the [**G**lobal **D**escriptor **T**able](gdt.md). 
