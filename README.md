# What is this project

This project is a very basic implementation of a 32bit operating system solely for the purpose of learning about OS development.
This is still very much in progress.

It can help you with the following topics:

1. Writing an MBR
2. Booting into 16bit real mode
3. Switching into 32 bit protected mode
4. Basic VGA driver
5. Basic Keyboard driver
6. Basic memory management implementation

# Prequisits

## OSX

1. x86_64-elf-gcc
2. x86_64-elf-ld
3. Make
4. QEmu

Just running `make` from the parent directory should start QEmu with everything running.

# Documentation

There is some that I wish to keep extending as I progress with adding further features to this OS.

[Boot process](docs/boot_process.md)

[Segmentation](docs/segmentation.md)

[GDT](docs/gdt.md)

[Interrupts]()

[Memory management]()



# Acknowledge 

A big thank you for Frank Rosner and his [OS dev blog](https://dev.to/frosnerd/writing-my-own-boot-loader-3mld) which was great help for getting me started.

[OS Dev Book](https://www.cs.bham.ac.uk/~exr/lectures/opsys/10_11/lectures/os-dev.pdf)

[OS Dev Wiki](https://wiki.osdev.org/Main_Page)


# Useful stuff

```
objdump -D -Mintel,i386 -b binary -m i386 foo.bin

xxd foo.bin
```
