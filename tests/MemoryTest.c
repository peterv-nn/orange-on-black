#include "CuTest/CuTest.h"
#include <string.h>
#include <stdio.h>
#include "../memory/memory.h"

void TestPoolIndexGetter(CuTest *tc) {
    CuAssertIntEquals(tc, 0, get_pool_index(0));
    CuAssertIntEquals(tc, 3, get_pool_index(8));
    CuAssertIntEquals(tc, 8, get_pool_index(230));
    CuAssertIntEquals(tc, 10, get_pool_index(1020));
}

void TestMemoryInit(CuTest *tc) {
    // Given
    // When
    dyn_mem_bucket_t** dynamic_test_memory_area = init_dynamic_memory();

    // Then
    CuAssertIntEquals(tc, 24, LST_NODE_SIZE);
    CuAssertIntEquals(tc, 79, dynamic_test_memory_area[0]->num_of_available_buckets);
    CuAssertIntEquals(tc, 76, dynamic_test_memory_area[1]->num_of_available_buckets);
    CuAssertIntEquals(tc, 71, dynamic_test_memory_area[2]->num_of_available_buckets);
    CuAssertIntEquals(tc, 62, dynamic_test_memory_area[3]->num_of_available_buckets);
    CuAssertIntEquals(tc, 49, dynamic_test_memory_area[4]->num_of_available_buckets);
    
    double_linked_list_node_t *node = dynamic_test_memory_area[1]->node;

    for(int bidx=0; bidx<2; ++bidx) {
        double_linked_list_node_t *node = dynamic_test_memory_area[bidx]->node;
        CuAssertPtrNotNull(tc, node);
        for(int i=0; i<dynamic_test_memory_area[bidx]->num_of_available_buckets; ++i) {
            CuAssertPtrNotNull(tc, node);
            node = node->next;
        }
    }
}

void TestMemoryAllocation(CuTest *tc) {
    // Given
    dyn_mem_bucket_t** dynamic_test_memory_area = init_dynamic_memory();

    // Then
    CuAssertIntEquals(tc, 71, dynamic_test_memory_area[2]->num_of_available_buckets);

    int* intPtr1 = mem_alloc(4);
    CuAssertIntEquals(tc, 70, dynamic_test_memory_area[2]->num_of_available_buckets);
    CuAssertPtrNotNull(tc, intPtr1);
    *intPtr1 = 20;
    CuAssertIntEquals(tc, 20, *intPtr1);
    CuAssertIntEquals(tc, 20, *((int*)dynamic_test_memory_area[2]->node));

    int* intPtr2 = mem_alloc(4);
    CuAssertIntEquals(tc, 69, dynamic_test_memory_area[2]->num_of_available_buckets);
    CuAssertPtrNotNull(tc, intPtr2);
    *intPtr2 = 30;
    CuAssertIntEquals(tc, 30, *intPtr2);
    CuAssertIntEquals(tc, 30, *((int*)dynamic_test_memory_area[2]->node->next));

    int* intArray = mem_alloc(4*(sizeof(int)));
    intArray[0] = 1;
    intArray[1] = 2;
    intArray[2] = 3;
    intArray[3] = 4;

    CuAssertIntEquals(tc, 2, intArray[1]);
    CuAssertIntEquals(tc, 48, dynamic_test_memory_area[4]->num_of_available_buckets);
    CuAssertIntEquals(tc, 1, *((uint8_t*)dynamic_test_memory_area[4]->node));
    CuAssertIntEquals(tc, 2, *((uint8_t*)dynamic_test_memory_area[4]->node + sizeof(int)));
    CuAssertPtrNotNull(tc, intArray);
}

void TestMemoryFree(CuTest *tc) {
    dyn_mem_bucket_t** dynamic_test_memory_area = init_dynamic_memory();

    int* intPtr1 = mem_alloc(sizeof(int));
    CuAssertIntEquals(tc, 70, dynamic_test_memory_area[2]->num_of_available_buckets);
    CuAssertPtrNotNull(tc, intPtr1);
    *intPtr1 = 20;
    CuAssertIntEquals(tc, 20, *intPtr1);
    CuAssertIntEquals(tc, 20, *((int*)dynamic_test_memory_area[2]->node));
    
    mem_free(intPtr1);
    CuAssertIntEquals(tc, 71, dynamic_test_memory_area[2]->num_of_available_buckets);
    CuAssertTrue(tc, !(dynamic_test_memory_area[2]->node->next->in_use));

    char* charPtr2 = mem_alloc(sizeof(char));
    CuAssertIntEquals(tc, 1, sizeof(*charPtr2));
}

CuSuite* MemoryTestSuite() {
    CuSuite* suite = CuSuiteNew();
    SUITE_ADD_TEST(suite, TestMemoryAllocation);
    SUITE_ADD_TEST(suite, TestPoolIndexGetter);
    SUITE_ADD_TEST(suite, TestMemoryInit);
    SUITE_ADD_TEST(suite, TestMemoryFree);
    return suite;
}


