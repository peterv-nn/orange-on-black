#include <stdio.h>

#include "CuTest/CuTest.h"
    
CuSuite* StrUtilSuite();
CuSuite* MemoryTestSuite();

void RunAllTests(void) {
	CuString *output = CuStringNew();
	CuSuite* suite = CuSuiteNew();
	
	CuSuiteAddSuite(suite, StrUtilSuite());
	// CuSuiteAddSuite(suite, MemoryTestSuite());

	CuSuiteRun(suite);
	CuSuiteSummary(suite, output);
	CuSuiteDetails(suite, output);
	printf("%s\n", output->buffer);
}

int main(void) {
	RunAllTests();
}
