#include <stdio.h>
#include "CuTest/CuTest.h"
#include "../utils/string/str.h"
#include "../memory/memory.h"


void TestIntToStrFunction(CuTest *tc) {
    dyn_mem_bucket_t** dynamic_test_memory_area = init_dynamic_memory();

    int a = 1;
    char* s = int_to_str(a);
    CuAssertStrEquals(tc, "1", s);

    int b = 12;
    s = int_to_str(b);
    CuAssertStrEquals(tc, "12", s);

    int c = 123456;
    s = int_to_str(c);
    CuAssertStrEquals(tc, "123456", s);

    int d = -1;
    s = int_to_str(d);
    CuAssertStrEquals(tc, "-1", s);

    int e = -123141;
    s = int_to_str(e);
    CuAssertStrEquals(tc, "-123141", s);
}

CuSuite* StrUtilSuite() {
    CuSuite* suite = CuSuiteNew();
    SUITE_ADD_TEST(suite, TestIntToStrFunction);
    return suite;
}