#pragma once

#include <stdint.h>
#include <stddef.h>
#include "linked_list.h"

#define BUCKET_SIZE_BYTE 4096
#define NUM_OF_MEMORY_BUCKETS 6

typedef struct dyn_mem_bucket_t {
    uint32_t num_of_available_buckets;
    uint32_t remaining_memory;
    double_linked_list_node_t* node;
} dyn_mem_bucket_t;

// memory API
dyn_mem_bucket_t** init_dynamic_memory();
void* mem_alloc(size_t size);
void mem_free(void* ptr);
uint32_t get_pool_index(size_t size);