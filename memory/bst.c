#include "bst.h"

// recursive insert into BST
// TODO: rewrite to non-recursive
static bst_node_t* insert_rec(bst_node_t *root, bst_node_t *parent, bst_node_t *free_mem_node) {
    if(root == NULL_PTR) {
        free_mem_node->parent = parent;
        return free_mem_node;
    }

    if(free_mem_node->size > root->size) {
        root->right = insert_rec(root->right, root, free_mem_node);
    } else {
        root->left = insert_rec(root->left, root, free_mem_node);
    }

    return root;
}

static bst_node_t* bst_minimum(bst_node_t *root) {
    bst_node_t *current = root;
    while(current->left != NULL_PTR) {
        current = current->left;
    }

    return current;
}

static void bst_transplant(bst_node_t *root, bst_node_t *node_remove, bst_node_t *node_replace) {
    if(node_remove->parent == NULL_PTR) {
        root = node_replace;
    }else if(node_remove == (node_remove->parent)->left) {
        (node_remove->parent)->left = node_replace;
    } else {
        (node_remove->parent)->right = node_replace;
    }

    if(node_replace != NULL_PTR) {
        node_replace->parent = node_remove->parent;
    }
}


bst_node_t* bst_insert(bst_node_t *root, bst_node_t *free_mem_node) {
    return insert_rec(root, NULL_PTR, free_mem_node);
}

void bst_remove(bst_node_t *root, bst_node_t *node) {
    if(node->left == NULL_PTR) {
        bst_transplant(root, node, node->right);
    } else if(node->right == NULL_PTR) {
        bst_transplant(root, node, node->left);
    } else {
        bst_node_t *sub_tree_min_node = bst_minimum(node->right);
        if(sub_tree_min_node->parent != node) {
            bst_transplant(root, sub_tree_min_node, sub_tree_min_node->right);
            sub_tree_min_node->right = node->right;
            (sub_tree_min_node->right)->parent = sub_tree_min_node;
        }

        bst_transplant(root, node, sub_tree_min_node);
        sub_tree_min_node->left = node->left;
        (sub_tree_min_node->left)->parent = node;
    }
}

// search the BST for node where min(node->size) >= size
bst_node_t* find_min_fit(bst_node_t *root, size_t size) {
    bst_node_t *current = root;
    bst_node_t *min_size_node_that_fits = (current->size > size) ? current : NULL_PTR;

    while(current != NULL_PTR && min_size_node_that_fits != NULL_PTR) {
        if(current->size >= size && current->size <= min_size_node_that_fits->size) {
            min_size_node_that_fits = current;
        }

        if(current->size > size) {
            current = current->left;
        } else {
            current = current->right;
        }
    }
    return min_size_node_that_fits;
}