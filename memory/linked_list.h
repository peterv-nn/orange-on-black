#pragma once

#include <stdint.h>
#include <stdbool.h>

#define NULL_PTR ((void*)0)
#define LST_NODE_SIZE sizeof(double_linked_list_node_t)

typedef struct double_linked_list_node_t {
    uint32_t size;
    bool in_use;
    struct double_linked_list_node_t *next;
    struct double_linked_list_node_t *prev;
} double_linked_list_node_t;


void lst_prepend(double_linked_list_node_t* head, double_linked_list_node_t* node);
void lst_append(double_linked_list_node_t* head, double_linked_list_node_t* node);
