#pragma once

#include <stdint.h>
#include <stddef.h>

#define NULL_PTR ((void*)0)
#define BST_NODE_SIZE sizeof(bst_node_t)

typedef struct bst_node_t {
    uint32_t size;
    struct bst_node_t *parent;
    struct bst_node_t *left;
    struct bst_node_t *right;

} bst_node_t;


static bst_node_t* insert_rec(bst_node_t *root, bst_node_t *parent, bst_node_t *node);
static void bst_transplant(bst_node_t *root, bst_node_t *node_remove, bst_node_t *node_replace);
static bst_node_t* bst_minimum(bst_node_t *root);


bst_node_t* bst_insert(bst_node_t *root, bst_node_t *free_mem_node);
void bst_remove(bst_node_t *root, bst_node_t *node);
bst_node_t* find_min_fit(bst_node_t *root, size_t size);