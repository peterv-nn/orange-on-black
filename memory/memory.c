#include "memory.h"

// char
static uint8_t memory_pool_1_byte[BUCKET_SIZE_BYTE];
// int, short
static uint8_t memory_pool_2_byte[BUCKET_SIZE_BYTE];
// int, float
static uint8_t memory_pool_4_byte[BUCKET_SIZE_BYTE];
// long, double
static uint8_t memory_pool_8_byte[BUCKET_SIZE_BYTE];
static uint8_t memory_pool_16_byte[BUCKET_SIZE_BYTE];
uint8_t memory_pool_32_byte[BUCKET_SIZE_BYTE];

dyn_mem_bucket_t dynamic_memory_bucket_pool[NUM_OF_MEMORY_BUCKETS];

dyn_mem_bucket_t* memory_pool[NUM_OF_MEMORY_BUCKETS];

dyn_mem_bucket_t** init_dynamic_memory() {
    for(int i=0; i<NUM_OF_MEMORY_BUCKETS; ++i) {
        uint32_t node_size = (i == 0) ? 1 : (2<<(i-1));
        memory_pool[i] = &dynamic_memory_bucket_pool[i];
        memory_pool[i]->remaining_memory = BUCKET_SIZE_BYTE - (LST_NODE_SIZE + node_size);
        memory_pool[i]->num_of_available_buckets = memory_pool[i]->remaining_memory / (LST_NODE_SIZE + node_size) - 1;
    }

    memory_pool[0]->node = (double_linked_list_node_t*) memory_pool_1_byte;
    memory_pool[1]->node = (double_linked_list_node_t*) memory_pool_2_byte;
    memory_pool[2]->node = (double_linked_list_node_t*) memory_pool_4_byte;
    memory_pool[3]->node = (double_linked_list_node_t*) memory_pool_8_byte;
    memory_pool[4]->node = (double_linked_list_node_t*) memory_pool_16_byte;
    memory_pool[5]->node = (double_linked_list_node_t*) memory_pool_32_byte;

    for(int i=0; i<NUM_OF_MEMORY_BUCKETS; ++i) {
        memory_pool[i]->node->next = NULL_PTR;
        memory_pool[i]->node->prev = NULL_PTR;
    }

    for(int bucket_idx=0; bucket_idx<NUM_OF_MEMORY_BUCKETS; ++bucket_idx) {
        uint32_t node_size = (bucket_idx == 0) ? 1 : 2<<(bucket_idx-1);
        uint32_t remaining = memory_pool[bucket_idx]->remaining_memory;
        uint32_t number_of_buckets = memory_pool[bucket_idx]->num_of_available_buckets;

        for(int lst_node_idx=0; lst_node_idx<number_of_buckets; ++lst_node_idx) {
            if(remaining > LST_NODE_SIZE + node_size) {
                // Allocate the node from the pool
                double_linked_list_node_t *node = (double_linked_list_node_t *)(
                    ((uint8_t *)memory_pool[bucket_idx]->node) + remaining - (node_size + LST_NODE_SIZE));
                node->size = node_size;
                node->next = NULL_PTR;
                node->prev = NULL_PTR;
                node->in_use = false;

                // Prepend the allocated node the node list
                lst_prepend(memory_pool[bucket_idx]->node, node);
                remaining -= LST_NODE_SIZE + node_size;
            }
        }
    }

    return memory_pool;
}

void* mem_alloc(size_t size) {
    int idx = get_pool_index(size);

    // Find first bucket that has enough free memory
    while(memory_pool[idx]->num_of_available_buckets <= 0) {
        ++idx;
    }
    
    double_linked_list_node_t *node = memory_pool[idx]->node;
    while(node->in_use == true) {
        node = node->next;
    }
    node->in_use = true;
    --memory_pool[idx]->num_of_available_buckets;

    return node;
}

void mem_free(void* ptr) {
    for(int bucket_idx = 0; bucket_idx < NUM_OF_MEMORY_BUCKETS; ++bucket_idx) {
        double_linked_list_node_t *node = memory_pool[bucket_idx]->node;
        while(node != NULL_PTR) {
            if(node == ptr) {
                node->in_use = false;
                ++memory_pool[bucket_idx]->num_of_available_buckets;
                return;
            }
            node = node->next;
        }
    }
    return;
}


// TODO: REVIEW
// First part will find the first power of 2 that is biffer than size,
// second will return the exponent.
// https://graphics.stanford.edu/~seander/bithacks.html#RoundUpPowerOf2
uint32_t get_pool_index(size_t size) {
    uint32_t v = size;
    v--;
    v |= v >> 1;
    v |= v >> 2;
    v |= v >> 4;
    v |= v >> 8;
    v |= v >> 16;
    v++;

    static const unsigned int b[] = {0xAAAAAAAA, 0xCCCCCCCC, 0xF0F0F0F0, 
                                     0xFF00FF00, 0xFFFF0000};
    register unsigned int r = (v & b[0]) != 0;
    for (int i = 4; i > 0; i--)
    {
        r |= ((v & b[i]) != 0) << i;
    }
    return r;
}
