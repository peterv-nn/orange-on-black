#include "linked_list.h"

void lst_prepend(double_linked_list_node_t* head, double_linked_list_node_t* node) {
    if(head->next != NULL_PTR) {
        node->next = head->next;
    }
    head->next = node;
    node->prev = head;
}

void lst_append(double_linked_list_node_t* head, double_linked_list_node_t* node) {
    double_linked_list_node_t* current = head;
    while(current->next != NULL_PTR) {
        current = current->next;
    }
    
    current->next = node;
}
