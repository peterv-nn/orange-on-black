#include "../drivers/vga.h"
#include "../utils/utils.h"
#include "../drivers/keyboard.h"
#include "../interrupt/isr.h"
#include "../memory/memory.h"
#include "../utils/string/str.h"

void start_kernel() {
    clr_screen();

    print("Welcome to OrangeOnBlackOS\n\n");
    print_str_coloured("Installing interrupt service routines (ISRs).\n", WHITE_ON_BLACK);
    isr_install();

    print_str_coloured("Enabling external interrupts.\n", WHITE_ON_BLACK);
    asm volatile("sti");

    print_str_coloured("Initializing keyboard (IRQ 1).\n", WHITE_ON_BLACK);
    init_keyboard();

    dyn_mem_bucket_t** memory_pool = init_dynamic_memory();

    int* intPtr1 = mem_alloc(sizeof(int));
    *intPtr1 = 9;
    
    print("value of ptr: ");
    println(int_to_str(*intPtr1));

    //int x = 5 / 0;
}