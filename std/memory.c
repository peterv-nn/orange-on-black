#include "memory.h"

void memory_copy(char *src, char *dst,  int num_of_bytes) {
    for(int i=0; i < num_of_bytes; ++i) {
        *(dst + i) = *(src + i);
    }
}