[bits 16]
switch_32bit:
    cli                     ; 1. disable interrupts
    xor ax, ax
    mov ds, ax
    lgdt [gdt_descriptor]   ; 2. load GDT descriptor
    mov eax, cr0
    or eax, 1              ; 3. enable protected mode
    mov cr0, eax
    jmp CODE_SEG:init_32bit ; 4. far jump

[bits 32]
init_32bit:
    mov ax, DATA_SEG        ; 5. update segment registers
    mov ds, ax
    mov ss, ax
    mov es, ax
    mov fs, ax
    mov gs, ax

    mov esp, 0x90000        ; 6. setup stack

    call BEGIN_32BIT        ; 7. move back to boot_sector.asm