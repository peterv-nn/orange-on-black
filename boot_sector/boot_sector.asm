[bits 16]
[org 0x7c00]
    call check_A20_set

    mov si, WELCOME_MESSAGE
    call print_string

    KERNEL_BASE   equ 0xffff      ; Loading the kernel at 1MB
    KERNEL_OFFSET equ 0x10        ; KERNEL_BASE * 16 + KERNEL_OFFSET = 0x100000
    
    mov [BOOT_DRIVE], dl
    mov bp, 0x9000
    mov sp, bp

    call load_kernel

    mov si, LOADING_KERNEL_DONE
    call print_string

    call switch_32bit

    jmp $


%include "boot_sector/print_string.asm"
%include "boot_sector/disk_load.asm"
%include "boot_sector/gdt.asm"
%include "boot_sector/switch_32bit.asm"

[bits 16]
load_kernel:
    ; kernel will be loaded at KERNEL_BASE*16:KERNEL_OFFSET
    ; es:bx
    mov bx, KERNEL_OFFSET
    mov dh, 32
    mov dl, [BOOT_DRIVE]
    mov ax, KERNEL_BASE
    mov es, ax
    xor ax, ax
    call disk_load
    ret

check_A20_set:
    mov ax, 0x2402
    int 0x15
    cmp al, 1
    jne a20_not_set

a20_set:    
    mov si, A20_SET_MESSAGE
    call print_string
    ret
a20_not_set:
    mov ax, 0x2401
    int 0x15
    jc a20_error
    ret
a20_error:
    mov si, A20_ERROR_MESSAGE
    call print_string
    ret

[bits 32]
BEGIN_32BIT:
    mov eax, KERNEL_BASE
    imul eax, 16
    add eax, KERNEL_OFFSET 
    call eax    ; Give control to the kernel
    jmp $       ; If the kernel returns, then endless loop

; Data 
WELCOME_MESSAGE:
    db "16bit real mode, booting...", 0

LOADING_KERNEL_DONE:
    db "Loading kernel from disk done.", 0

A20_SET_MESSAGE:
    db "A20 is enabled.", 0

A20_ERROR_MESSAGE:
    db "A20 can not be set.", 0

BOOT_DRIVE:
    db 0 

times 510-($-$$) db 0 ; padding with zeroes
dw 0xaa55             ; the magic number


