#pragma once

#define VIDEO_ADDRESS 0xb8000
#define MAX_ROWS 25
#define MAX_COLS 80

#define WHITE_ON_BLACK 0x0f
#define ORANGE_ON_BLACK 0x0c

#define DEFAULT_COLOUR ORANGE_ON_BLACK

// VGA device I/O ports
#define VGA_CTRL_REG 0x3d4
#define VGA_DATA_REG 0x3d5
#define VGA_CURSOR_OFFSET_REG_HIGH 0x0e
#define VGA_CURSOR_OFFSET_REG_LOW 0x0f


int get_offset(int row, int col);
int get_row_from_offset(int offset);
int get_cursor();

void set_cursor(int offset);
void print_char_at(char character, int offset, unsigned char colour);
int move_offset_to_new_line(int offset);

int scroll_ln(int offset);
void clr_screen();