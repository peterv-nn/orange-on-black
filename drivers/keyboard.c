#include "keyboard.h"
#include "../in_out/port.h"
#include "../utils/utils.h"
#include "../interrupt/isr.h"

const char sc_ascii[] = {'X', 'E', '1', '2', '3', '4', '5', '6','7', '8', '9', '0', '-', '=', 'B', 
                         'T', 'q', 'w', 'e', 'r', 't', 'y','u', 'i', 'o', 'p', '[', ']', 'E', 
                         'X', 'a', 's', 'd', 'f', 'g',
                         'h', 'j', 'k', 'l', ';', '\'', '`', 'X', '\\', 'z', 'x', 'c', 'v',
                         'b', 'n', 'm', ',', '.', '/', 'X', 'X', 'X', ' '};

static void keyboard_callback(registers_t *regs) {
    uint8_t scancode = port_byte_read(0x60);
    
    // filter out key release
    if(scancode > 57) return;

    switch((int) scancode) {
        case 1:
            print("ESC");
        break;
        case 14:
            print("BACK SPACE");
        break;
        case 15:
            print("TAB");
        break;
        case 28:
            print("\n");
        break;
        default:
            char letter = sc_ascii[(int) scancode];
            char str[2] = {letter, '\0'};
            print(str);
        }
}

void init_keyboard() {
    register_interrupt_handler(33, keyboard_callback);
}