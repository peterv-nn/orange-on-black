#include "vga.h"
#include "../in_out/port.h"
#include "../std/memory.h"
#include <stdint.h>


int get_offset(int row, int col) {
    return (row * MAX_COLS + col) * 2;
}

int get_row_from_offset(int offset) {
    return offset / (2 * MAX_COLS);
}

int move_offset_to_new_line(int offset) {
    return get_offset(get_row_from_offset(offset) + 1, 0);
}

int get_cursor() {
    // setting the ctrl register to the cursor offset high byte
    port_byte_write(VGA_CTRL_REG, 14);
    // reading the offset high byte, number of rows
    int offset = port_byte_read(VGA_DATA_REG) << 8;

    // setting the ctrl register to the cursor offset low byte
    port_byte_write(VGA_CTRL_REG, 15);
    // reading the offset low byte, number of cols
    offset += port_byte_read(VGA_DATA_REG);

    // offset will be the number of characters, since each cell holds two bytes we need to 
    // multiply by 2 to get the correct video memory offset
    return offset * 2;
}

void set_cursor(int offset) {
    offset /= 2;
    port_byte_write(VGA_CTRL_REG, 14);
    port_byte_write(VGA_DATA_REG, (unsigned char) (offset >> 8));
    port_byte_write(VGA_CTRL_REG, 15);
    port_byte_write(VGA_DATA_REG, (unsigned char) (offset & 0xff));
}

void print_char_at(char character, int offset, unsigned char colour) {
    unsigned char *video_memory = (unsigned char *) VIDEO_ADDRESS;
    video_memory[offset] = character;
    video_memory[offset + 1] = colour;
}

int scroll_ln(int offset) {
    memory_copy((char *) (get_offset(1, 0) + VIDEO_ADDRESS), 
                (char *) (get_offset(0, 0) + VIDEO_ADDRESS), 
                MAX_COLS * (MAX_ROWS - 1) * 2);

    for(int col = 0; col < MAX_COLS; ++col) {
        print_char_at(' ', get_offset(MAX_ROWS - 1, col), ORANGE_ON_BLACK);
    }

    return offset - 2 * MAX_COLS;
}

void clr_screen() {
    for(int i = 0; i < MAX_COLS * MAX_ROWS; ++i) {
        print_char_at(' ', i * 2, ORANGE_ON_BLACK);
    }
    set_cursor(get_offset(0,0));
}